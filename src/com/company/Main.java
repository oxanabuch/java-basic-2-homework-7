package com.company;

public class Main {

    public static void main(String[] args) {

        Shoppers shopper1 = new Shoppers();
        shopper1.name = "Jameson";
        shopper1.age = 15;
        shopper1.cash = 5_000;
        shopper1.income = 20_000;
        shopper1.printListInfo();

        Shoppers shopper2 = new Shoppers();
        shopper2.name = "Jonson";
        shopper2.age = 45;
        shopper2.cash = 35_000;
        shopper2.income = 50_000;
        shopper2.printListInfo();

        Shoppers shopper3 = new Shoppers();
        shopper3.name = "Peterson";
        shopper3.age = 50;
        shopper3.cash = 100_000;
        shopper3.income = 70_000;
        shopper3.printListInfo();

        Cars car = new Cars();
        car.brand = "Toyota";
        car.model = "Corolla";
        car.color = "Black";
        car.price = 65_000;
        car.availability = true;
        car.number = 5;
        car.printInfo();

        Loan loan = new Loan();
        loan.ready = true;

        System.out.println();

        if (shopper1.cash >= car.price) {
            String result3 = " Congratulations, you can pick up your car";
            System.out.println(shopper1.name + "!" + result3);
            System.out.println();
        }
        if (shopper2.cash >= car.price) {
            String result3 = " Congratulations, you can pick up your car";
            System.out.println(shopper2.name + "!" + result3);
            System.out.println();
        }
        if (shopper3.cash >= car.price) {
            String result3 = " Congratulations, you can pick up your car";
            System.out.println(shopper3.name + "!" + result3);
            System.out.println();
        }

        if (shopper1.cash < car.price && shopper1.age > 18) {
            int temp2 = car.price - shopper1.cash;
            String result4 = " You can take a loan for the amount that is missing: ";
            System.out.println(shopper1.name + "!" + result4 + temp2 + " for 3 years");
            System.out.println();
            if (loan.ready = true) {
                loan.PrintLoanInfo();
                System.out.println();
                System.out.println(shopper1.name + "!" + "You have been given a loan");
                System.out.println("Monthly payment: " + (temp2 / 36) + " + interest on the loan");
                String result5 = " Congratulations, you can pick up your car";
                System.out.println(shopper1.name + "!" + result5);
                System.out.println();
            }
        }
        if (shopper2.cash < car.price) {
            int temp2 = car.price - shopper2.cash;
            String result4 = " You can take a loan for the amount that is missing: ";
            System.out.println(shopper2.name + "!" + result4 + temp2 + " for 3 years");
            System.out.println();
            if (loan.ready = true) {
                loan.PrintLoanInfo();
                System.out.println();
                System.out.println(shopper2.name + "!" + "You have been given a loan");
                System.out.println("Monthly payment: " + (temp2 / 36) + " + interest on the loan");
                String result5 = " Congratulations, you can pick up your car";
                System.out.println(shopper2.name + "!" + result5);
                System.out.println();
            }
        }
        if (shopper3.cash < car.price) {
            int temp2 = car.price - shopper3.cash;
            String result4 = " You can take a loan for the amount that is missing: ";
            System.out.println(shopper3.name + "!" + result4 + temp2 + " for 3 years");
            System.out.println();
            if (loan.ready = true) {
                loan.PrintLoanInfo();
                System.out.println();
                System.out.println(shopper3.name + "!" + "You have been given a loan");
                System.out.println("Monthly payment: " + (temp2 / 36) + " + interest on the loan");
                String result5 = " Congratulations, you can pick up your car";
                System.out.println(shopper3.name + "!" + result5);
                System.out.println();
            }
        }
    }
}



